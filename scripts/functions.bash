
#!/usr/bin/bash

NR_CORES=$(($(grep -c ^processor /proc/cpuinfo) + 1))

function __prepare_build() {
    if [ -d "$1" ]; then 
	rm -rf $1/*
    else
	mkdir -p $1
    fi
    cd $1
}

function build_debug() {
    __prepare_build $1
    cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Debug ../..
    make -j $NR_CORES
}

function build_release() {
    __prepare_build $1
    cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ../..
    make -j $NR_CORES
}

function execute_tests() {
    cd $1
    ctest -V
}
