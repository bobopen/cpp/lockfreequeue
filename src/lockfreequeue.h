#ifndef LOCK_FREE_QUEUE_H
#define LOCK_FREE_QUEUE_H

#define DEFAULT_QUEUE_SIZE 4096

#include <atomic>
#include <cstdlib>
#include <stdexcept>
#include <vector>

namespace mutils {
/**
 * This class represents a thread safe queue which allows threads to
 * enqueue and dequeue elements without any conflict. Enqueuing and
 * dequeuing happen without any locks. Please be aware that enqueuing
 * is not unlimited.
 */
template <typename T> class lockfreequeue
{
  public:
  /**
   * The constructor which sets the maximum number of elements that
   * the constructing queue allows to contain to 4096.
   */
  lockfreequeue();
  /**
   *
   * @param uint32_t the length of the queue with a maximum of
   * (2^32) - 1
   * @throws std::runtime_error it throws an exception if there is not
   * enough memory to created the underlying container.
   */

  /**
   * The constructor which accepts a maximum number of elements that
   * the constructing queue allows to contain .
   *
   * @param length The number of elements that the queue allows to
   * contain.
   */
  lockfreequeue(uint32_t length);

  /**
   * The destructor sets the internal variables to zero and frees the
   * reserved memory.
   */
  ~lockfreequeue();

  /**
   * It enqueues the given element into the queue. Please be aware
   * that the calling thread will spins endless when the queue is full
   * and never a place becomes free.
   *
   * @param element the element that must be enqueued.
   * @return true when the enqueuing is finished.
   */
  bool enqueue(T&& element);

  /**
   * It enqueues the given element into the queue. As long as the
   * enqueuing has not happened the caller tries up to the given max
   * to enqueue the element.
   *
   * @param element The element that should be enqueued.
   * @param max The maximum number of attempts to enqueue the given element.
   * @return true if the element is enqueued, otherwise false.
   */
  bool tryEnqueue(T&& element, uint16_t max);

  /**
   * It dequeues the first element of the queue. Please be aware that
   * the caller keeps endless spinning when there is no element in the
   * queue.
   *
   * @param element The contains the first element of the queue when
   * the function has returned.

   * @return it returns true when the function has returned.
   */

  bool dequeue(T& element);

  /**
   * It dequeues the first element of the queue.
   *
   * @param element It contains the first element of the queue when an
   * element has been dequeued after given number of attempts.
   * @param max The maximum number of attempts caller tries to dequeue
   * an element from the queue.
   *
   * @return True if an element has been dequeued, otherwise false.
   */
  bool tryDequeue(T& element, uint16_t max);

  private:
  std::atomic<uint32_t> _head;
  std::atomic<uint32_t> _tail;
  std::atomic<uint32_t> _capacity;
  T* _container;
};
}

template <typename T>
mutils::lockfreequeue<T>::lockfreequeue()
    : lockfreequeue(DEFAULT_QUEUE_SIZE)
{}

template <typename T>
mutils::lockfreequeue<T>::lockfreequeue(uint32_t size)
    : _capacity(size + 1)
{
  _container = (T*)malloc(sizeof(T) * _capacity);
  if (_container == NULL) {
    throw std::runtime_error("Lack of memory to construct the queue");
  }
  _head.store(0, std::memory_order_release);
  _tail.store(0, std::memory_order_release);
}

template <typename T> mutils::lockfreequeue<T>::~lockfreequeue()
{
  _capacity.store(0, std::memory_order_release);
  _head.store(0, std::memory_order_release);
  _tail.store(0, std::memory_order_release);
  free(_container);
}

template <typename T> bool mutils::lockfreequeue<T>::enqueue(T&& e)
{
  while (true) {
    auto index = _tail.load(std::memory_order_acquire);
    auto head = _head.load(std::memory_order_acquire);
    if ((index + 1) % _capacity != head
        && _tail.compare_exchange_weak(index, (index + 1) % _capacity,
               std::memory_order_release, std::memory_order_acquire)) {
      new (_container + index) T(std::forward<T>(e));
      return true;
    }
  }
}

template <typename T>
bool mutils::lockfreequeue<T>::tryEnqueue(T&& e, uint16_t tries)
{
  for (int16_t i = 0; i < tries; ++i) {
    auto index = _tail.load(std::memory_order_acquire);
    auto head = _head.load(std::memory_order_acquire);
    if ((index + 1) % _capacity != head
        && _tail.compare_exchange_weak(index, (index + 1) % _capacity,
               std::memory_order_release, std::memory_order_acquire)) {
      new (_container + index) T(std::forward(e));
      return true;
    }
  }
  return false;
}

template <typename T> bool mutils::lockfreequeue<T>::dequeue(T& element)
{
  while (true) {
    auto index = _head.load(std::memory_order_acquire);
    auto tail = _tail.load(std::memory_order_acquire);
    if (index != tail
        && _head.compare_exchange_weak(index, (index + 1) % _capacity,
               std::memory_order_release, std::memory_order_acquire)) {
      element = std::move(*(_container + index));
      return true;
    }
  }
}

template <typename T>
bool mutils::lockfreequeue<T>::tryDequeue(T& element, uint16_t tries)
{
  for (uint16_t i = 0; i < tries; ++i) {
    auto index = _head.load(std::memory_order_acquire);
    auto tail = _tail.load(std::memory_order_acquire);
    if (index != tail
        && _head.compare_exchange_weak(index, (index + 1) % _capacity,
               std::memory_order_release, std::memory_order_acquire)) {
      element = std::move(*(_container + index));
      return true;
    }
  }
  return false;
}

#endif
