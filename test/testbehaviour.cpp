#define private public

#include "lockfreequeue.h"

#include <catch.hpp>
#include <iostream>
#include <thread>

SCENARIO("Concurrent enqueues to a queue do not interfere")
{
  mutils::lockfreequeue<int> q(900);
  GIVEN("a lockfree queue of a length of 900 and three threads enqueueing "
        "elements to it")
  {
    std::thread t0(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
            q.enqueue(std::forward<int>(i));
          }
        },
        std::ref(q), 0, 300);
    std::thread t1(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
            q.enqueue(std::forward<int>(i));
          }
        },
        std::ref(q), 300, 600);
    std::thread t2(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
            q.enqueue(std::forward<int>(i));
          }
        },
        std::ref(q), 600, 900);
    WHEN("three threads end their work")
    {
      t0.join();
      t1.join();
      t2.join();
      THEN("all elements are added to the queue correctly")
      {
        CHECK(q._capacity == 901);
        CHECK(q._tail == 900);
        CHECK(q._head == 0);
        int counter = 0;
        for (uint16_t i = 0; i < 900; i++) {
          if (q._container[i] == 0 || q._container[i] == 300
              || q._container[i] == 600) {
            counter++;
          }
        }
        CHECK(counter == 3);
      }
    }
  }
}

SCENARIO("Concurrent enqueues and dequeue do not interfere")
{
  mutils::lockfreequeue<int> q(900);
  GIVEN("a lockfree queue of a length of 900 and three threads enqueueing "
        "elements and three threads dequeing the same number of elements "
        "concurrently")
  {
    std::thread t0(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
            q.enqueue(std::forward<int>(i));
          }
        },
        std::ref(q), 0, 300);
    std::thread t1(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
            q.enqueue(std::forward<int>(i));
          }
        },
        std::ref(q), 0, 300);
    std::thread t2(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
            q.enqueue(std::forward<int>(i));
          }
        },
        std::ref(q), 0, 300);

    std::thread d0(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
	    int j;
            q.dequeue(j);
          }
        },
        std::ref(q), 0, 300);
    std::thread d1(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
	    int j;
            q.dequeue(j);
          }
        },
        std::ref(q), 0, 300);
    std::thread d2(
        [&](mutils::lockfreequeue<int>& q, int start, int end) {
          for (int i = start; i < end; i++) {
	    int j;
            q.dequeue(j);
          }
        },
        std::ref(q), 0, 300);

    WHEN("all threads end their work")
    {
      t0.join();
      t1.join();
      t2.join();
      d0.join();
      d1.join();
      d2.join();

      THEN("No element exists in the queue")
      {
        int j;
	CHECK(!q.tryDequeue(j, 1));
        CHECK(q._head == q._tail);
      }
    }
  }
}

SCENARIO("overriden queue  places and its effect on previous values")
{
  struct Obj
  {
    int _value0;
    double _value1;
  };
  GIVEN("a queue with a size of two")
  {
    mutils::lockfreequeue<Obj> q(2);
    WHEN("two elements are enqueued")
    {
      q.enqueue({ 1, 1.1 });
      q.enqueue({ 2, 2.2 });
      THEN("two elements are dequeued and saved locally")
      {
	Obj f, s;
        q.dequeue(f);
        q.dequeue(s);
        CHECK(f._value0 == 1);
        CHECK(f._value1 == 1.1);
        CHECK(s._value0 == 2);
        CHECK(s._value1 == 2.2);
        AND_WHEN("two new elements are enqueued")
        {
          q.enqueue({ 3, 3.3 });
          q.enqueue({ 4, 4.4 });
          THEN("the last enqueued elements are dequeued")
          {
	    Obj ff, ss;
            q.dequeue(ff);
            q.dequeue(ss);
            CHECK(ff._value0 == 3);
            CHECK(ff._value1 == 3.3);
            CHECK(ss._value0 == 4);
            CHECK(ss._value1 == 4.4);
            AND_THEN("all local variables have their own value")
            {
              CHECK(f._value0 == 1);
              CHECK(f._value1 == 1.1);
              CHECK(s._value0 == 2);
              CHECK(s._value1 == 2.2);
              CHECK(ff._value0 == 3);
              CHECK(ff._value1 == 3.3);
              CHECK(ss._value0 == 4);
              CHECK(ss._value1 == 4.4);
            }
          }
        }
      }
    }
  }
}
